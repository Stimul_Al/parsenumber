# Parse Number

#### Test task for JazzTeam company

The application converts a numerical value into a written (in Russian). 
Converts positive numbers from ***10 ^ 96***.

**Example:**  
>Ввод: ***1521042***

>Вывод: ***один миллион пятьсот двадцать одна тысяча сорок два***

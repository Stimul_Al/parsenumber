package by.babashev.parser.parser;

import by.babashev.parser.entity.Number;
import by.babashev.parser.util.Form;
import by.babashev.parser.util.FormThousand;
import lombok.extern.slf4j.Slf4j;

import java.math.BigInteger;

import static java.lang.Math.pow;

@Slf4j
public class Parser {

    private static final String THOUSAND = "1000";
    private static final int TEN = 10;
    private static final int ZERO = 0;
    private static final int ONE = 1;

    private Number number;

    public String parse(Number number) {
        this.number = number;
        log.info("число с которым будем работать: " + number);
        StringBuilder parseOfNumber = new StringBuilder();

        while (this.number.getPowerThousand() >= ZERO) {
            int nextNumber = getNextNumber();

            log.info("MAIN_PARSE: получили число, с которым будем работать: " + nextNumber);
            parseOfNumber.append(parseNumber(nextNumber));

            if (nextNumber != ZERO) {
                if (this.number.getPowerThousand() == ONE) {
                    parseOfNumber = checkEnd(parseOfNumber.toString());
                }

                if (((nextNumber / TEN) % TEN) == ONE) {
                    parseOfNumber.append(addPowerThousand(this.number.getPowerThousand(), ZERO));
                } else {
                    parseOfNumber.append(addPowerThousand(this.number.getPowerThousand(), nextNumber % TEN));
                }
            }

            reduceNumber();

            log.info("MAIN_PARSE: Уменьшили число до: " + this.number.getNumber());
            minusPowerThousand();
            log.info("MAIN_PARSE: уменьшили степень тысяч до: " + this.number.getPowerThousand());
        }

        if (parseOfNumber.length() == ZERO) {
            return "ноль";
        }

        return parseOfNumber.toString().trim();
    }

    private StringBuilder checkEnd(String str) {
        StringBuilder stringBuilder = new StringBuilder();
        if (str.startsWith("два", str.length() - 4)) {
            return stringBuilder.append(str, ZERO, str.length() - 2).append("е ");
        } else if (str.startsWith("один", str.length() - 5)) {
            return stringBuilder.append(str, ZERO, str.length() - 3).append("на ");
        }
        return stringBuilder.append(str);
    }

    private String addPowerThousand(int powerThousand, int lastDigit) {
        FormThousand formThousand = new FormThousand();

        return formThousand.getThousandForm(powerThousand, lastDigit);
    }

    private int getNextNumber() {
        if (number.getPowerThousand() == ZERO) {
            return number.getNumber().mod(new BigInteger(THOUSAND)).intValue();
        }
        return number.getNumber().divide(new BigInteger(THOUSAND).pow(number.getPowerThousand())).intValue();
    }

    private void minusPowerThousand() {
        int powerThousand = number.getPowerThousand();
        powerThousand--;
        number.setPowerThousand(powerThousand);
    }

    private void reduceNumber() {
        BigInteger bigInteger = number.getNumber();
        bigInteger = bigInteger.mod(new BigInteger(THOUSAND).pow(number.getPowerThousand()));
        number.setNumber(bigInteger);
    }

    private String parseNumber(int number) {
        log.info("PARSER_TENS: Работаем в парсере с числом: " + number);

        StringBuilder stringBuilder = new StringBuilder();
        Form form = new Form();

        int powerTen = getPowerTen(number);

        while (powerTen >= ZERO) {
            int digit = (int) (number / pow(TEN, powerTen));

            if (powerTen == ONE && digit == ONE) {
                digit = (int) (number % pow(TEN, powerTen));
                powerTen -= 2;
            }
            log.info("PARSER_TENS: цифра с которой работаем: " + digit);

            stringBuilder.append(form.getForm(powerTen, digit));
            log.info("PARSER_TENS: Имеем слово: " + stringBuilder);

            number %= pow(TEN, powerTen);
            log.info("PARSER_TENS: сократили число: " + number);

            powerTen--;
            log.info("PARSER_TENS: уменьшили степень: " + powerTen);
        }

        return stringBuilder.toString();
    }

    private int getPowerTen(int number) {
        int count = ZERO;
        while (number > ZERO) {
            count += number / TEN == ZERO ? ZERO : ONE;
            number /= TEN;
        }
        return count;
    }
}

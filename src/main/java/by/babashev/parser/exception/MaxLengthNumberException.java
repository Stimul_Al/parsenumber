package by.babashev.parser.exception;

public class MaxLengthNumberException extends Exception {

    public MaxLengthNumberException() {
        super("Length exceeds possible. Possible length '96')");
    }

}

package by.babashev.parser.exception;

public class WrongNumberFormatException extends Exception{

    public WrongNumberFormatException() {
        super("Incorrect recording format!");
    }
}

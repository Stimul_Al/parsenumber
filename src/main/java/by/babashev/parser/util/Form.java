package by.babashev.parser.util;

import java.util.Map;

import static by.babashev.parser.util.Util.initMap;

public class Form {

    private final static String PATH_NUMBER = "src/main/resources/number.txt";

    private final Map<Integer, String> mapDigits;

    public Form() {
        this.mapDigits = initMap(PATH_NUMBER);
    }

    public String getForm(int powerTen, int digit) {
        String word = mapDigits.get(digit);

        if (powerTen == 2) {
            return getWordHundred(digit, word);
        }
        if (powerTen == 1) {
            return getWordTens(digit, word);
        }
        if (powerTen == -1) {
            return getWordOneTens(digit, word);
        }
        return getWordUnits(digit, word);
    }

    private String getWordHundred(int digit, String word) {
        if (digit == 1) {
            return "сто ";
        } else if (digit == 2) {
            return "двести ";
        } else if (digit < 5) {
            return word + "ста ";
        }
        return word + "сот ";
    }

    private String getWordTens(int digit, String word) {
        if (digit == 0) {
            return "";
        } else if (digit == 2 || digit == 3) {
            return word + "дцать ";
        } else if (digit == 4) {
            return "сорок ";
        } else if (digit == 9) {
            return "девяносто ";
        }
        return word + "десят ";
    }

    private String getWordOneTens(int digit, String word) {
        if (digit == 0) {
            return "десять ";
        }
        if (digit == 2 || digit == 3 || digit == 1) {
            return word.replace("а", "е") + "надцать ";
        }
        return word.substring(0, word.length() - 1) + "надцать ";
    }

    private String getWordUnits(int digit, String word) {
        if (digit == 0) {
            return "";
        }
        return word + " ";
    }
}

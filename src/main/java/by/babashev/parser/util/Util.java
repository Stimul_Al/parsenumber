package by.babashev.parser.util;

import by.babashev.parser.exception.MaxLengthNumberException;
import by.babashev.parser.exception.WrongNumberFormatException;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

@Slf4j
public class Util {

    private final static Integer maxLengthNumber = 96;

    public static String writeDigit() {
        String digitToParse = null;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            boolean check = false;
            while (!check) {
                try {
                    digitToParse = reader.readLine();
                    checkDigit(digitToParse);
                    check = true;
                } catch (MaxLengthNumberException | WrongNumberFormatException e) {
                    log.error(e.getMessage());
                }
            }
        } catch (IOException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return digitToParse;
    }

    private static void checkDigit(String digitToParse) throws MaxLengthNumberException, WrongNumberFormatException {
        if (digitToParse.length() > maxLengthNumber) {
            throw new MaxLengthNumberException();
        }

        for (Character ch : digitToParse.toCharArray()) {
            if (!Character.isDigit(ch)) {
                throw new WrongNumberFormatException();
            }
        }
    }

    public static HashMap<Integer, String> initMap(String path) {
        HashMap<Integer, String> digits = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            while (reader.ready()) {
                String str = reader.readLine();
                Integer digit = Integer.parseInt(str.substring(0, str.indexOf(" ")));
                String name = str.substring(str.indexOf(" ") + 1);
                digits.put(digit, name);
            }
        } catch (IOException e) {
            log.error("File doesn't exist!");
            e.printStackTrace();
        }
        return digits;
    }
}

package by.babashev.parser.util;

import java.util.Map;

import static by.babashev.parser.util.Util.initMap;

public class FormThousand {

    private final static String PATH_POWER_OF_THOUSAND = "src/main/resources/power-thousand.txt";

    private final Map<Integer, String> mapPowerThousand;

    public FormThousand() {
        this.mapPowerThousand = initMap(PATH_POWER_OF_THOUSAND);
    }

    public String getThousandForm(int powerThousand, int lastDigit) {
        String word = mapPowerThousand.get(powerThousand);

        if (powerThousand == 0) {
            return "";
        } else if (powerThousand == 1) {
            return getWordFirstPowerThousand(lastDigit, word);
        }
        return getWordOtherPowerThousand(lastDigit, word);
    }

    private String getWordFirstPowerThousand(int lastDigit, String word) {
        if (lastDigit == 1) {
            return word + "а ";
        } else if (lastDigit < 5 && lastDigit > 1) {
            return word + "и ";
        }
        return word + " ";
    }

    private String getWordOtherPowerThousand(int lastDigit, String word) {
        if (lastDigit == 1) {
            return word + " ";
        } else if (lastDigit < 5 && lastDigit > 1) {
            return word + "а ";
        }
        return word + "ов ";
    }
}

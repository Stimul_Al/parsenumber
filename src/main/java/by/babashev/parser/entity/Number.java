package by.babashev.parser.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@NoArgsConstructor
public class Number {

    private static final String THOUSAND = "1000";
    private static final String ZERO = "0";

    private BigInteger number;
    private int powerThousand;

    public void saveNumber(String number) {
        this.number = new BigInteger(number);
        this.powerThousand = getPowerThousandThisNumber();
    }

    private int getPowerThousandThisNumber() {
        BigInteger bigInteger = number;
        int count = 0;
        while (!bigInteger.divide(new BigInteger(THOUSAND)).equals(new BigInteger(ZERO))) {
            bigInteger = bigInteger.divide(new BigInteger(THOUSAND));
            count++;
        }
        return count;
    }
}

package by.babashev.parser;

import by.babashev.parser.entity.Number;
import by.babashev.parser.parser.Parser;

import static by.babashev.parser.util.Util.writeDigit;

public class Main {

    public static void main(String[] args) {
        Parser parser = new Parser();
        Number number = new Number();

        System.out.println("Enter the number: ");

        number.saveNumber(writeDigit());

        System.out.println("--------------------------\n" +
                "Answer: " + parser.parse(number));
    }
}

import by.babashev.parser.util.Form;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FormTest {

    private final Form form = new Form();

    @Test
    void parseNumberHundred_happyPath() {
        //given
        int digit = 5;
        int powerTen = 2;
        String actualWord = "пятьсот";

        //when
        String expectedWord = form.getForm(powerTen, digit).trim();

        //then
        Assertions.assertEquals(expectedWord, actualWord);
    }

    @Test
    void parseNumberTen_happyPath() {
        //given
        int number = 4;
        int powerTen = 1;
        String actualWord = "сорок";

        //when
        String expectedWord = form.getForm(powerTen, number).trim();

        //then
        Assertions.assertEquals(expectedWord, actualWord);
    }

    @Test
    void parseNumberTens_happyPath() {
        //given
        int number = 1;
        int powerTen = -1;
        String actualWord = "одиннадцать";

        //when
        String expectedWord = form.getForm(powerTen, number).trim();

        //then
        Assertions.assertEquals(expectedWord, actualWord);
    }

    @Test
    void parseNumberUnits_happyPath() {
        //given
        int digit = 2;
        int powerTen = 0;
        String actualWord = "два";

        //when
        String expectedWord = form.getForm(powerTen, digit).trim();

        //then
        Assertions.assertEquals(expectedWord, actualWord);
    }
}

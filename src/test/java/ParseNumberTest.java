import by.babashev.parser.entity.Number;
import by.babashev.parser.parser.Parser;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParseNumberTest {

    private final Parser parser = new Parser();
    private final Number number = new Number();


    @Test
    public void test() throws IOException {
        //given
        HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream("src/test/resources/test.xls"));
        HSSFSheet sheet = workbook.getSheetAt(0);

        long digit = 0;
        String name = null;
        for (Row row : sheet) {
            for (Cell cell : row) {
                if (cell.getCellType() == CellType.NUMERIC) {
                    digit = (long) cell.getNumericCellValue();
                } else if (cell.getCellType() == CellType.STRING) {
                    name = cell.getStringCellValue();
                } else {
                    return;
                }
            }

            number.saveNumber(digit + "");

            //when
            String exceptedSentence = parser.parse(number);

            //then
            assertEquals(exceptedSentence, name);
        }
    }
}

import by.babashev.parser.util.FormThousand;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FormThousandTest {

    private final FormThousand formThousand = new FormThousand();

    @Test
    void getFirstFormWordOnePowerThousand (){
        //given
        int lastDigit = 2;
        int powerThousand = 1;
        String actualWord = "тысячи";

        //when
        String expectedWord = formThousand.getThousandForm(powerThousand, lastDigit).trim();

        //then
        Assertions.assertEquals(expectedWord, actualWord);
    }

    @Test
    void getSecondFormWordOnePowerThousand (){
        //given
        int lastDigit = 1;
        int powerThousand = 1;
        String actualWord = "тысяча";

        //when
        String expectedWord = formThousand.getThousandForm(powerThousand, lastDigit).trim();

        //then
        Assertions.assertEquals(expectedWord, actualWord);
    }

    @Test
    void getThirdFormWordOnePowerThousand (){
        //given
        int lastDigit = 5;
        int powerThousand = 1;
        String actualWord = "тысяч";

        //when
        String expectedWord = formThousand.getThousandForm(powerThousand, lastDigit).trim();

        //then
        Assertions.assertEquals(expectedWord, actualWord);
    }

    @Test
    void getFirstFormWordTwoPowerThousand (){
        //given
        int lastDigit = 2;
        int powerThousand = 2;
        String actualWord = "миллиона";

        //when
        String expectedWord = formThousand.getThousandForm(powerThousand, lastDigit).trim();

        //then
        Assertions.assertEquals(expectedWord, actualWord);
    }

    @Test
    void getSecondFormWordTwoPowerThousand (){
        //given
        int lastDigit = 1;
        int powerThousand = 2;
        String actualWord = "миллион";

        //when
        String expectedWord = formThousand.getThousandForm(powerThousand, lastDigit).trim();

        //then
        Assertions.assertEquals(expectedWord, actualWord);
    }

    @Test
    void getThirdFormWordTwoPowerThousand (){
        //given
        int lastDigit = 5;
        int powerThousand = 2;
        String actualWord = "миллионов";

        //when
        String expectedWord = formThousand.getThousandForm(powerThousand, lastDigit).trim();

        //then
        Assertions.assertEquals(expectedWord, actualWord);
    }
}
